/**
 * @author Omar Chatila
 * @file Util.h
 * @brief Various utility functions and structs
 */

#pragma once

#include "QColor"
#include "cstdlib"
#include <fstream>
#include <random>
#include <set>
#include <sstream>
#include <unordered_set>

using namespace std;

enum Quadrant { NORTH_WEST, NORTH_EAST, SOUTH_WEST, SOUTH_EAST };

static double getColorDistance(QColor a, QColor b) {
    double redDiff = pow(a.red() - b.red(), 2);
    double greenDiff = pow(a.green() - b.green(), 2);
    double blueDiff = pow(a.blue() - b.blue(), 2);
    double alphaDiff = pow(a.alpha() - b.alpha(), 2);
    return sqrt(redDiff + greenDiff + blueDiff + alphaDiff);
}

static bool areColorsClose(QColor a, QColor b) {
    return getColorDistance(a, b) < 10.00;
}

/**
 * @brief A struct representing a 2D Pixel with x and y coordinates.
 */
struct Pixel
{
    int x; /**< The x-coordinate of the Pixel. */
    int y; /**< The y-coordinate of the Pixel. */
    QColor color;

    /**
     * @brief Overloaded << operator to stream the Pixel information.
     * @param os The output stream.
     * @param Pixel The Pixel instance to be streamed.
     * @return The output stream.
     */
    friend std::ostream &operator<<(std::ostream &os, const Pixel &Pixel)
    {
        return os << "[" << (Pixel.x) << ":" << (Pixel.y) << "]";
    }

    /**
     * @brief Overloaded == operator to check if two Pixels are equal.
     * @param other The other Pixel to compare with.
     * @return True if the x and y coordinates of the Pixels are equal; false otherwise.
     */
    bool operator==(const Pixel &other) const { return areColorsClose(color, other.color); }

    /**
     * @brief Overloaded != operator to check if two Pixels are equal.
     * @param other The other Pixel to compare with.
     * @return True if the x and y coordinates of the Pixels are not equal; false otherwise.
     */
    bool operator!=(const Pixel &other) const { return !areColorsClose(color, other.color); }
};

namespace std {

/**
     * @brief Hash specialization for the Pixel struct.
     */
template<>
struct hash<Pixel>
{
    /**
         * @brief Operator to generate a unique hash for a Pixel.
         * @param p The Pixel for which the hash is generated.
         * @return The hash value.
         */
    size_t operator()(const Pixel &p) const
    {
        // Combine the hashes of x and y to generate a unique hash for the Pixel
        return hash<int>()(p.x) ^ (hash<int>()(p.y) << 1);
    }
};
} // namespace std

/**
 * Area struct for Quadtree squares and KD-Tree rectangles
 */
struct Area
{
    friend std::ostream &operator<<(std::ostream &os, const Area &area)
    {
        return os << "[" << (area.xMin) << ":" << (area.xMax) << "] : [" << (area.yMin) << ":"
                  << (area.yMax) << ']';
    }

    /**
     * @brief Overloaded == operator to check if two Areas are equal.
     * @param other The other Pixel to compare with.
     * @return True if areas are equal; false otherwise.
     */
    bool operator==(const Area &other) const
    {
        return xMin == other.xMin && yMin == other.yMin && xMax == other.xMax && yMax == other.yMax;
    }

    int xMin, xMax, yMin, yMax;
};

struct ColorArea
{
    Area rect;
    QColor color;
};

/**
 * @brief splits area into 4 quadrants and returns them
 * @param area Area to be split
 * @param xMid mid x-coordinate
 * @param yMid mid y-coordinate
 * @return array of the 4 quadrants
 */
inline Area *splitArea(Area &area, int xMid, int yMid)
{
    Area *areas = (Area *) std::malloc(sizeof(Area) << 2);
    areas[NORTH_EAST] = Area{xMid, area.xMax, yMid, area.yMax};
    areas[NORTH_WEST] = Area{area.xMin, xMid, yMid, area.yMax};
    areas[SOUTH_WEST] = Area{area.xMin, xMid, area.yMin, yMid};
    areas[SOUTH_EAST] = Area{xMid, area.xMax, area.yMin, yMid};
    return areas;
}

/**
 * @brief Checks if two areas intersect each other
 *
 * Note: also returns true if borders overlap but interception area is 0
 *
 * @param first  area
 * @param other  area
 * @return true if they intersect each other, otherwise false
 */
inline bool intersects(Area &first, Area &other)
{
    return first.xMin <= other.xMax && first.xMax >= other.xMin && first.yMax >= other.yMin
           && first.yMin <= other.yMax;
}

/**
 * @brief Checks if first area intersects the second
 * @param container Area possibly containing contained
 * @param contained Area possibly contained by container
 * @return True if container contains contained, false otherwise
 */
inline bool containsArea(Area &container, Area &contained)
{
    return container.xMin <= contained.xMin && container.xMax >= contained.xMax
           && container.yMin <= contained.yMin && container.yMax >= contained.yMax;
}

/**
 * @brief Checks if area contains Pixel
 * @param area
 * @param Pixel
 * @return true if area contains Pixel, otherwise false
 */
inline bool containsPixel(Area &area, Pixel &Pixel)
{
    return (Pixel.x >= area.xMin && Pixel.y >= area.yMin && Pixel.x <= area.xMax
            && Pixel.y <= area.yMax);
}

inline Area intersection(Area &first, Area &other) {
    int xMin = max(first.xMin, other.xMin);
    int xMax = min(first.xMax, other.xMax);
    int yMin = max(first.yMin, other.yMin);
    int yMax = min(first.yMax, other.yMax);

    return Area{xMin, xMax, yMin, yMax};
}
