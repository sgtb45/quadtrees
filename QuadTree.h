/**
 * @author Omar Chatila
 * @file QuadTree.h
 * @brief Implementation of a Pixel-QuadTree with leaf capacity 1
 */

#ifndef QUADKDBENCH_QUADTREE_H
#define QUADKDBENCH_QUADTREE_H

#include "Util.h"
#include "qpixmap.h"
#include <iomanip>
#include <list>

/**
 * @brief A class representing a QuadTree data structure.
 */
class QuadTree
{
    /**
     * @brief Overloaded << operator to stream the QuadTree information.
     * @param os The output stream.
     * @param quadTree The QuadTree instance to be streamed.
     * @return The output stream.
     */
    friend std::ostream &operator<<(std::ostream &os, const QuadTree &quadTree)
    {
        os << std::fixed << std::setprecision(1);
        if (!quadTree.elements.empty())
            return os << "A:" << quadTree.square << "elements:" << quadTree.elements.front()
                      << "\n";
        return os << "A:" << quadTree.square << "\n";
    }

private:
    QuadTree *children[4]{}; /**< Pixelers to the 4 children of the QuadTree node. */
    Area square;             /**< The area covered by the QuadTree node. */
    vector<Pixel> elements;  /**< The vector of Pixels associated with the QuadTree node. */
    QColor mixedColor; /**< The color representing the node. if leaf, then color of its pixels, otherwise mix of all colors. */

    /**
     * @brief Determines the quadrant of a Pixel based on the given split coordinates.
     * @param Pixel The Pixel to be located.
     * @param xMid The x-coordinate of the midPixel.
     * @param yMid The y-coordinate of the midPixel.
     * @return The quadrant index (0 to 3) where the Pixel belongs.
     */
    static int determineQuadrant(const Pixel &Pixel, int xMid, int yMid);

    /**
     * @brief Locates the quadrant of the QuadTree based on the specified coordinates.
     * @param PixelX The x-coordinate of the Pixel to be located.
     * @param PixelY The y-coordinate of the Pixel to be located.
     * @param current The current QuadTree node being considered.
     * @return The QuadTree node representing the located quadrant.
     */
    static QuadTree *locateQuadrant(Pixel &Pixel, QuadTree *current);

    /**
     * @brief Subdivides the square and Pixels into 4 partitions and creates 4 children
     */
    void subdivide();

    /**
     * @brief adds all leaves of tree to vector
     * @param noce current node
     * @param leaves if quadtree
     */
    void gather_leaves_helper(QuadTree *node, vector<QuadTree *> &leaves);

    /**
     * @brief Collects node to a given depth
     * @param node current node
     * @param nodes list of nodes
     * @param level given level
     */
    void nodes_at_level_helper(QuadTree *node, vector<QuadTree *> &nodes, int level);

public:
    /**
     * @brief Constructs a QuadTree with the specified square area and elements.
     * @param square The square area covered by the QuadTree.
     * @param elements The vector of Pixels contained in the QuadTree.
     */
    QuadTree(Area square, vector<Pixel> &elements);

    /**
     * @brief Creates Quadtree from image by setting pixels as Pixel set and
     * image width and height as area
     * @param image
     */
    QuadTree(const QPixmap &image);

    /**
     * @brief destroys the Quadtree and deallocates memory
     */
    ~QuadTree();

    /**
     * @brief Checks if a node is a leaf
     * @return True if node is leaf, false otherwise
     */
    bool isNodeLeaf();

    /**
     * @brief Returns mix (mean) of all colors of given color-vector
     * @param vector containing colors to be mixed
     * @return mixed color
     */
    static QColor getMixedColor(vector<Pixel> colors);

    /**
     * @brief Checks if a node represents monochromatic color block
     * @return True, if elements only contains one color, false otherwise
     */
    bool isMixedNode();

    /**
     * @brief Calculates height of the Quadtree
     * @return The height of the Quadtree
     */
    int getHeight();

    /**
     * @brief Builds the Quadtree
     * Builds Quadtree by subdividing its square and Pixels into 4 Quadrants. Splits as long as elements.size() > 1
     */
    void buildTree();

    /**
     * @brief returns the color represented by a node
     * @return color of current node
     */
    QColor getMixedColor();

    /**
     * @brief Gets all leaves of the quadtree
     * @return vector containing all leaves
     */
    vector<QuadTree *> gatherLeaves();

    /**
     * @param queryRectangle Rectangle that contains Pixels of interest
     * @return list<Pixel> of Pixels contained by queryRectangle
     */
    list<ColorArea> query(Area &queryRectangle);

    /**
     * @brief Checks if a given Pixel is contained by the Quadtree
     * @param Pixel
     * @return True if Quadtree contains Pixel, false otherwise
     */
    bool contains(Pixel &Pixel);

    /**
     * Checks if Quadtree is empty, meaning it does not contains any elements
     * @return True if Quadtree is empty, false otherwise
     */
    bool isEmpty();

    /**
     * Adds a given Pixel to the Quadtree
     * @param Pixel Pixel to be added
     */
    void add(Pixel &Pixel);

    /**
     * @brief getSquare
     * @return this.square
     */
    Area getSquare();

    /**
     * @brief returns nodes to a given depth
     * @param level depth
     * @return vector of nodes
     */
    vector<QuadTree *> nodes_at_level(int level);

    /**
     * @brief Returns all Areas + colors of leaves from subtree
     * @return list of ColorArea obj
     */
    list<ColorArea> report_subtree();
};

#endif //QUADKDBENCH_QUADTREE_H
