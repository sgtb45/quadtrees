#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QRubberBand>
#include "QuadTree.h"
#include "qpainter.h"
#include "qtimer.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    int IMAGE_HEIGHT = 512;
    int IMAGE_WIDTH = 512;

private slots:
    void on_selectImage_clicked();

    void on_pushButton_clicked();

    void drawNextRectangle();

    void on_elliptic_button_clicked();

    void on_stop_button_clicked();

    void on_save_button_clicked();

protected:
    void mousePressEvent(QMouseEvent *event) override;

    void mouseMoveEvent(QMouseEvent *event) override;

    void mouseReleaseEvent(QMouseEvent *event) override;

    bool rect_out_of_bounds(QMouseEvent *event);

private:
    Ui::MainWindow *ui;
    QuadTree *image_quadtree{};
    int currentIndex;
    QTimer *animationTimer;
    vector<QuadTree *> leaves;
    QPixmap pixmap;
    QPainter painter;
    QPoint origin;
    QRubberBand rubberBand;
};
#endif // MAINWINDOW_H
