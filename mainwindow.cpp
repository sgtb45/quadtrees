#include "mainwindow.h"
#include <QFileDialog>
#include <QTimer>
#include "./ui_mainwindow.h"
#include "qpainter.h"
#include <QStandardPaths>
#include <iostream>
#include "QImageWriter"
#include "QMouseEvent"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , currentIndex(0)
    , rubberBand(QRubberBand::Rectangle, this)
{
    ui->setupUi(this);
    ui->stop_button->setVisible(false);
    ui->save_button->setDisabled(false);
    animationTimer = new QTimer(this);
    connect(animationTimer, SIGNAL(timeout()), this, SLOT(drawNextRectangle()));
}

MainWindow::~MainWindow()
{
    painter.end();
    delete ui;
    delete image_quadtree;
}

void MainWindow::on_selectImage_clicked()
{
    QString imagePath = QFileDialog::getOpenFileName(this,
                                                     tr("Open Image"),
                                                     QDir::homePath(),
                                                     tr("Image Files (*.png *.jpg *.bmp)"));
    if (!imagePath.isEmpty()) {
        QPixmap image(imagePath);
        IMAGE_HEIGHT = image.height();
        IMAGE_WIDTH = image.width();
        this->image_quadtree = new QuadTree(image);
        ui->selectedImageView->setPixmap(
            image.scaled(ui->selectedImageView->size(), Qt::KeepAspectRatio));


        auto start = std::chrono::high_resolution_clock::now();
        image_quadtree->buildTree();
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration_us = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
        auto time = duration_us.count();

        leaves = image_quadtree->gatherLeaves();
        int tree_height = image_quadtree->getHeight();

        double compressed_size = leaves.size();
        int compression_rate = (1 - compressed_size / (IMAGE_WIDTH * IMAGE_HEIGHT)) * 100;

        QString qstr = QString::fromStdString("RQT-Height: " + to_string(tree_height)
                                              + ". Compr. rate: " + to_string(compression_rate)
                                              + "% | Encode: " + to_string(time) + " ms");
        ui->infoLabel->setText(QString(qstr));
        this->pixmap = QPixmap(IMAGE_WIDTH, IMAGE_HEIGHT);
        this->pixmap.fill(Qt::white);
        this->painter.begin(&pixmap);
        // Set the QPixmap as the image for the QLabel
        //ui->writableImage->setGeometry(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT);
        ui->writableImage->setScaledContents(true);
        ui->writableImage->setPixmap(pixmap);
    }
}

void MainWindow::on_pushButton_clicked()
{
    // Start the animation when the button is clicked
    currentIndex = 0;
    animationTimer->start(1); // Adjust the interval as needed
    ui->stop_button->setVisible(true);
}

void MainWindow::drawNextRectangle()
{
    // Draw the rectangle at the current index
    for (int i = 0; i < 100; i++) {
        if (currentIndex + i >= leaves.size()) break;
        auto node = leaves[currentIndex + i];
        painter.setBrush(node->getMixedColor());
        Area square = node->getSquare();
        int x = square.xMin;
        int y = square.yMin;
        int width = square.xMax - square.xMin;
        int height = square.yMax - square.yMin;
        if (width <= 2) {
            painter.setPen(Qt::NoPen);
        } else {
            painter.setPen(Qt::DashLine);
        }
        painter.drawRect(x, y, width, height);
        QString qstr = QString::fromStdString("(" + to_string(currentIndex) + "/" + to_string(leaves.size()) + ")");
        ui->infoLabel->setText(qstr);
        // Increment the current index
    }
    currentIndex += 100;

    ui->writableImage->setPixmap(pixmap);

    // Check if all rectangles have been drawn
    if (currentIndex >= leaves.size()) {
        // Stop the animation timer
        animationTimer->stop();
        // End painting
        painter.end();
        ui->stop_button->setVisible(false);
        ui->save_button->setVisible(true);
    }
}

void MainWindow::on_stop_button_clicked()
{
    animationTimer->stop();
    for (; currentIndex < leaves.size(); currentIndex++) {
        auto node = leaves[currentIndex];
        Area square = node->getSquare();
        int x = square.xMin;
        int y = square.yMin;
        int width = square.xMax - square.xMin;
        int height = square.yMax - square.yMin;
        painter.setBrush(node->getMixedColor());
        if (width <= 2) {
            painter.setPen(Qt::NoPen);
        } else {
            painter.setPen(Qt::DashLine);
        }
        painter.drawRect(x, y, width, height);
    }
    ui->writableImage->setPixmap(pixmap);
    painter.end();
    ui->stop_button->setVisible(false);
    ui->infoLabel->setText("Animation completed!");
    ui->save_button->setDisabled(false);
}

void MainWindow::on_elliptic_button_clicked()
{
    vector<QuadTree *> nodesAtLevel = image_quadtree->nodes_at_level(6);
    this->pixmap.fill(Qt::black);
    this->painter.begin(&pixmap);
    painter.setPen(Qt::NoPen);
    for (auto node : nodesAtLevel) {
        painter.setBrush(node->getMixedColor());
        Area square = node->getSquare();
        int x = square.xMin;
        int y = square.yMin;
        int width = square.xMax - square.xMin;
        int height = square.yMax - square.yMin;
        painter.setBrush(node->getMixedColor());
        painter.drawEllipse(x, y, width, height);
    }
    ui->writableImage->setPixmap(pixmap);
    painter.end();
    ui->save_button->setDisabled(false);
}

void MainWindow::on_save_button_clicked()
{
    QString imagePath = QFileDialog::getSaveFileName(this,
                                                     tr("Save Image"),
                                                     QStandardPaths::writableLocation(QStandardPaths::PicturesLocation),
                                                     tr("JPEG (*.jpg .jpeg);;PNG (.png)"));
    QImage image = pixmap.toImage();
    QImageWriter writer(imagePath);
    writer.setFormat("PNG");

    // Write image to file
    if (writer.write(image)) {
        ui->infoLabel->setText("Image saved successfully");
    } else {
        ui->infoLabel->setText("Something went wrong");
    }
}

void MainWindow::mousePressEvent(QMouseEvent *event) {
    if (!rect_out_of_bounds(event)) {
        origin = event->pos();
        rubberBand.setGeometry(QRect(origin, QSize()));
        rubberBand.show();
    }
}

void MainWindow::mouseMoveEvent(QMouseEvent *event) {
    if (rect_out_of_bounds(event)) return;
    rubberBand.setGeometry(QRect(origin, event->pos()).normalized());
}

void MainWindow::mouseReleaseEvent(QMouseEvent *event) {
    pixmap.fill(Qt::white);
    if (rect_out_of_bounds(event)) return;
    painter.begin(&pixmap);
    QPoint releasePos = event->pos();
    rubberBand.hide();

    double width_ratio =  IMAGE_WIDTH / (double)ui->writableImage->width();
    double height_ratio =  IMAGE_HEIGHT / (double)ui->writableImage->height();

    cout << width_ratio << "-" << height_ratio << endl;

    int x = std::min(origin.x(), releasePos.x()) * width_ratio;
    int y = std::max(origin.y(), releasePos.y()) * height_ratio;
    int width = std::abs(releasePos.x() - origin.x()) * width_ratio;
    int height = std::abs(releasePos.y() - origin.y()) * height_ratio;
    Area crop{x, x + width, y - height, y};

    std::cout << crop << endl;

    // Query the QuadTree for the cropped rectangles
    std::list<ColorArea> cropped_rectangles = image_quadtree->query(crop);

    // Draw the cropped rectangles
    pixmap.fill(Qt::transparent);
    this->painter.begin(&pixmap);
    painter.setPen(Qt::NoPen);
    for (auto color_area : cropped_rectangles) {
        Area square = color_area.rect;
        painter.setBrush(color_area.color);
        painter.drawRect(square.xMin, square.yMin, square.xMax - square.xMin, square.yMax - square.yMin);
    }
    ui->writableImage->setPixmap(pixmap);
    painter.end();
}

bool MainWindow::rect_out_of_bounds(QMouseEvent *event) {
    return event->pos().x() < ui->writableImage->x() || event->pos().x() > ui->writableImage->x() + ui->writableImage->width()
           || event->pos().y() < ui->writableImage->y() || event->pos().y() > ui->writableImage->y() + ui->writableImage->height();
}
