//
// Created by omarc on 12/01/2024.
//

/**
 * @file QuadTree.cpp
 * @brief Implementation of a Pixel-QuadTree
 */

#include "QuadTree.h"
#include <queue>

QuadTree::QuadTree(Area square, vector<Pixel> &elements)
{
    this->square = square;
    this->elements = elements;
}

QuadTree::QuadTree(const QPixmap &image)
{
    this->square = {0, image.width(), 0, image.height()};
    for (int x = 0; x < image.width(); x++) {
        for (int y = 0; y < image.height(); y++) {
            this->elements.push_back({x, y, image.toImage().pixel(x, y)});
        }
    }
}

QuadTree::~QuadTree()
{
    this->elements.clear();
    for (auto &i : children) {
        delete i;
    }
}

int QuadTree::getHeight()
{
    // Leaf has height one
    if (isNodeLeaf()) {
        return 1;
    }
    // Calculates heights of 4 subtrees
    int h1 = this->children[NORTH_EAST]->getHeight();
    int h2 = this->children[NORTH_WEST]->getHeight();
    int h3 = this->children[SOUTH_WEST]->getHeight();
    int h4 = this->children[SOUTH_EAST]->getHeight();
    // Height is the longest height of subtrees
    int maxHeight = max(max(h1, h2), max(h3, h4));
    return maxHeight + 1;
}

bool QuadTree::isNodeLeaf()
{
    return this->children[0] == nullptr;
}

void QuadTree::buildTree()
{
    // Subdivide node if it contains more than one color
    if (this->isMixedNode()) {
        this->mixedColor = getMixedColor(this->elements);
        // Subdivides Pixels and quadrants into four quadrants
        subdivide();
        // Recursively build the 4 children
        children[NORTH_EAST]->buildTree();
        children[NORTH_WEST]->buildTree();
        children[SOUTH_WEST]->buildTree();
        children[SOUTH_EAST]->buildTree();
    } else if (!this->elements.empty()) {
        this->mixedColor = elements[0].color;
    }
}

vector<QuadTree *> QuadTree::gatherLeaves()
{
    vector<QuadTree *> leaves;
    gather_leaves_helper(this, leaves);
    return leaves;
}

void QuadTree::gather_leaves_helper(QuadTree *node, vector<QuadTree *> &leaves)
{
    if (node->isNodeLeaf()) {
        leaves.push_back(node);
    } else {
        for (int i = 0; i < 4; ++i) {
            if (node->children[i] != nullptr) {
                gather_leaves_helper(node->children[i], leaves);
            }
        }
    }
}

bool QuadTree::isMixedNode()
{
    for (auto point : this->elements) {
        if (this->elements[0] != point) {
            return true;
        }
    }
    return false;
}

QColor QuadTree::getMixedColor(vector<Pixel> colors)
{
    int n = colors.size();
    int red = 0, green = 0, blue = 0, opacity = 0;
    for (auto pixel : colors) {
        QColor color = pixel.color;
        red += color.red();
        green += color.green();
        blue += color.blue();
        opacity += color.alpha();
    }
    return QColor(red / n, green / n, blue / n, opacity / n);
}

void QuadTree::subdivide()
{
    // calculate vertical and horizontal split coordinates of the square
    int xMid = (this->square.xMin + this->square.xMax) / 2;
    int yMid = (this->square.yMin + this->square.yMax) / 2;
    // create the 4 quadrants by splitting the square
    Area *quadrants = splitArea(this->square, xMid, yMid);
    // create 4 vectors for each quadrant
    vector<Pixel> childrenElements[4];
    // push Pixels of Quadtree node to the corresponding lists
    for (const auto &Pixel : elements) {
        int quadrant = determineQuadrant(Pixel, xMid, yMid);
        childrenElements[quadrant].push_back(Pixel);
    }
    // Create the 4 children with the corresponding squares and elements
    for (int i = 0; i < 4; i++) {
        children[i] = new QuadTree(quadrants[i], childrenElements[i]);
    }
    free(quadrants);
}

QuadTree *QuadTree::locateQuadrant(Pixel &Pixel, QuadTree *current)
{
    int centerX = (current->square.xMin + current->square.xMax) / 2.0;
    int centerY = (current->square.yMin + current->square.yMax) / 2.0;
    int quadrant = determineQuadrant(Pixel, centerX, centerY);
    return current->children[quadrant];
}

std::list<ColorArea> QuadTree::query(Area &queryRectangle)
{
    std::list<ColorArea> result;
    if (this->isNodeLeaf()) {
        if (intersects(queryRectangle, this->square)) {
            result.push_back(ColorArea{intersection(this->square, queryRectangle), this->mixedColor});
        }
        return result;
    } else if (containsArea(queryRectangle, this->square)) {
        auto sub_result = this->report_subtree();
        result.insert(result.end(), sub_result.begin(), sub_result.end());
        return result;
    }
    // For each child: check if its area intersects queryRectangle. If yes, recursively call query on them
    for (auto child : this->children) {
        if (child != nullptr && intersects(queryRectangle, child->square)) {
            std::list<ColorArea> childResult = child->query(queryRectangle);
            result.insert(result.end(), childResult.begin(), childResult.end());
        }
    }
    return result;
}

bool QuadTree::contains(Pixel &Pixel)
{
    // Traverses the tree from root to leaf by comparing Pixel with coordinates of quadrants
    QuadTree *current = this;
    while (!current->isNodeLeaf()) {
        current = locateQuadrant(Pixel, current);
    }
    // return true iff Pixel of leaf is equal to Pixel
    return !current->elements.empty() && current->elements[0] == Pixel;
}

int QuadTree::determineQuadrant(const Pixel &Pixel, int xMid, int yMid)
{
    // Determines quadrant fast via enum indices
    int quadrant = 0b00; // LSB: W/E. MSB: S/N
    if (Pixel.x > xMid) {
        quadrant |= 0b01; // East
    }
    if (Pixel.y <= yMid) {
        quadrant |= 0b10; // South
    }
    return quadrant;
}

bool QuadTree::isEmpty()
{
    return this->elements.empty();
}

void QuadTree::add(Pixel &Pixel)
{
    QuadTree *current = this;
    // push Pixel to root elements if empty
    if (current->isEmpty()) {
        current->elements.push_back(Pixel);
        return;
    }

    // traverse to the correct location
    while (!current->isNodeLeaf()) {
        current = locateQuadrant(Pixel, current);
    }
    current->elements.push_back(Pixel);

    // if current has too many elements, subdivide
    if (current->elements.size() > 1) {
        current->subdivide();
    }
}

void QuadTree::nodes_at_level_helper(QuadTree *node, vector<QuadTree *> &nodes, int level)
{
    if (node != nullptr && level >= 0) {
        if (node->isNodeLeaf() || level == 0)
            nodes.push_back(node);
        for (auto quadrant : node->children) {
            nodes_at_level_helper(quadrant, nodes, level - 1);
        }
    }
}

vector<QuadTree *> QuadTree::nodes_at_level(int level)
{
    vector<QuadTree *> result;
    nodes_at_level_helper(this, result, level);
    return result;
}

QColor QuadTree::getMixedColor()
{
    return this->mixedColor;
}

Area QuadTree::getSquare()
{
    return this->square;
}

list<ColorArea> QuadTree::report_subtree() {
    list<ColorArea> result;
    if (this->isNodeLeaf()) {
        result.push_back({this->square, this->mixedColor});
    } else {
        for (auto child : this->children) {
            if (child != nullptr){
                list<ColorArea> child_res = child->report_subtree();
                result.insert(result.end(), child_res.begin(), child_res.end());
            }
        }
    }
    return result;
}
